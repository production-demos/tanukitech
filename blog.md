title: "Automatically manage tags/labels for your Terraform Code with Terratag"
author: Itzik Gan-Baruch
author_gitlab: iganbaruch
categories: engineering
tags: "DevOps, GitOps"
description: This blog addresses how you can do that easily and automatically when using Terraform and Terratag (an open source project by env0) on top of the Gitlab CI/CD platform.
image_title: /images/blogimages/2021-terratag-env0/blog-image.png
twitter_text: "Automatically manage tags/labels for your Terraform Code with Terratag"
---

This blog post is a joint work of [Itzik Gan-Baruch](https://www.linkedin.com/in/itzikg/), Sr. Technical Marketing Manager at GitLab, and [Omry Hay](https://www.linkedin.com/in/omryhay/) the co-founder and CTO of [env0](http://www.env0.com).


## Overview

[Terraform](https://www.terraform.io/) is the most widely adopted IaC framework out there. It is an open source project that is maintained by HashiCorp, and was launched in 2014 based on HashiCorp configuration language (HCL). It is a CLI tool that can help manage and provision external resources such as public cloud infrastructure, private cloud infrastructure, network appliances, and SaaS and PaaS vendors. All major clouds are supported where AWS, Azure and GCP have an official provider that are maintained internally by the HashiCorp Terraform team. 

[GitLab](https://about.gitlab.com/) is an industry-leader DevOps platform. Not long ago they announced the ability to control Terraform deployments, remote state management, private module registry and merge request integration for Terraform. This gives you a wide range of solutions for running CI/CD for your Terraform code and managing it on a large scale.

All major cloud providers support tagging / labeling for most of their resources using their Terraform provider, to help you manage your infrastructure in a better and more efficient way. Let’s take a deep dive into the importance of tagging and labeling your Infrastructure as code when using a public cloud provider, and see how you can do that easily and automatically when using Terraform and [Terratag](https://terratag.io/) (an open-source project by [env0](http://www.env0.com)) on top of the Gitlab CI/CD platform, with simple code examples for an end-to-end solution.


## Automatically manage tags/labels for your Terraform Code 
Let’s take a deep dive into the importance of tagging and labeling your Infrastructure as code when using a public cloud provider. This blog addresses how you can do that easily and automatically when using Terraform and [Terratag](https://terratag.io/) on top of the Gitlab CI/CD platform, with simple code examples for an end-to-end solution.


### Why tags / labels are so important

All major cloud providers allow tagging (or labeling) cloud resources. Moreover, they all encourage you to tag/label so you get the most out of their complementary services which allow  you to manage budgets, set up powerful automation algorithms, and unlock insights offered both by the cloud providers themselves and independent third parties.

Harnessing powerful infrastructure-as-code frameworks such as Terraform to define and tag your cloud resources allows for useful dynamic tag applications on various verticals from technical and operations, to business needs.

### The problem with tagging today

As your infrastructure grows, the need to repeat the manual process of tagging over and over for dozens or even hundreds of cloud resources can become a real hassle. But that’s just the tip of the iceberg. Manual tagging processes fail in other important ways:

* **Standards are hard to maintain if they’re not enforced** - Your entire team needs to be onboard, keeping an eye out for newly added cloud resources, making sure they include those tags or you may miss some significant resources when acting on that metadata later.

* **Harder to change** - Applying changes to tag structure across the board quickly becomes unmanageable.

* **Metadata can obscure what’s important** - While all this tagging metadata is extremely useful for slicing and dicing later, having it everywhere on your resources pollutes your lovely IaC making it much more cumbersome and harder to maintain.

* **Migration** - What if you already have plenty of Terraform modules with cloud resources which weren’t tagged to begin with? Trying to tag them all now can be painstaking work.

Infrastructure-as-code is, well, just code. And that’s the case with any code - code repetition makes it harder to fix errors, apply enhancements, make adjustments and maintain readability. As tagging is a cross-cutting concern, the lack of proper layering or aspect control makes it harder to retrofit existing solutions.

### Terratag to the rescue

[Terratag](https://terratag.io/) is an open source project by [env0](https://www.env0.com/) that enables you to automatically tag / label all the resources in your Terraform code. It also automatically tags all of your Terraform sub-modules, even if they don’t expose tags as an input. 

Terratag is a CLI tool that supports all the major cloud providers like AWS, Google Cloud Platform and Microsoft Azure and solves the complicated problem of tagging resources across applications at scale. It eliminates the risk of human error, can retroactively tag IaC resources previously deployed, and helps you easily utilize the tags for cost management, organization, reporting, etc.

### Running Terraform in Gitlab

Gitlab offers a wide range of tools for Terraform, starting with a [managed remote state](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html), running your deployment with [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/), [Terraform private module registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/index.html#publish-a-terraform-module-by-using-cicd) and also [integration in Merge Requests](https://docs.gitlab.com/ee/user/infrastructure/mr_integration.html) and getting Terraform plan output information into a merge request. So in this blog post we will use Gitlab CI/CD to deploy a Terraform repository into Google Cloud Platform and let Gitlab manage our remote state.

### Combining them 

So let’s take a deep dive into the implementation details and see how we can combine these tools with ease, starting with building the deployment of our Terraform code using Gitlab and then see the results in Google Cloud platform.

### Terraform code with Gitlab as a backend

In this example, we’re using Terraform to deploy a simple VPC and a VM into Google Cloud Platform. We will use Gitlab Terraform backend configuration which is based on the Terraform [HTTP backend](https://www.terraform.io/docs/language/settings/backends/http.html). The beauty of that is that when you’re running this inside Gitlab CI/CD, you don’t need to add any configuration regarding authentication. Gitlab will automatically set up all the relevant configuration for your backend according to the project it’s running in. 

You can check the code [here](https://gitlab.com/env0/terratag-blog-post/-/tree/main) 

### Setup variables

As our Terraform code needs some variables in order to run, we can set these up using Gitlab CI/CD variables. Under your Gitlab Project, go to Settings > CI/CD and expand the variable section. We will need to add 3 variables:

* `GOOGLE_CREDENTIALS` - This variable value should be the JSON of your Google Cloud service account -- [See this documentation](https://cloud.google.com/iam/docs/creating-managing-service-account-keys) on how to create a service account key.

* `TF_VAR_project_id` - Your Google Cloud project ID.

* `TF_VAR_machine_type`  The VM type you would like to create.

![tg_1](/images/blogimages/2021-terratag-env0/tg_1.png)

### Setup Gitlab CI/CD

Setting up a Gitlab CI/CD for Terraform is really easy, and all you have to do is add a simple file into your repository called `.gitlab-ci.yml` and add a configuration for each step of your Terraform deployment. We’re going to add the following steps to our pipeline:

* **Plan** - This step will run the `terraform init` and `terraform plan` commands and in the middle will also run Terratag to tag all the relevant resources. At the end it will also output the Terraform plan as a `JSON` file and create an artifact.

* **Apply** - This step will run the `terraform apply` command. It depends on the plan to finish successfully. This step is a manual step so we can check the plan before applying the changes.

[https://gitlab.com/env0/terratag-blog-post/-/blob/main/.gitlab-ci.yml](https://gitlab.com/env0/terratag-blog-post/-/blob/main/.gitlab-ci.yml)

Since Terratag scans the entire Terraform code, including any Terraform modules you might be using, we need to run the `terraform init` command before we run the Terratag command, as the init command will download all the relevant modules so that Terratag can scan them.

We can see 2 resources in this code:

* `google_compute_network` - This resource sets up the VPC. Terratag will not apply labels to it as it doesn’t allow labels as you can see in [its documentation](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network).

* `google_compute_instance` - This resource sets up the VM. Terratag applies the label that we defined.

Here is the output of Terratag on this Terraform code:

![tg_2](/images/blogimages/2021-terratag-env0/tg_2.png)

This is what this pipeline will look like in the Gitlab UI. When the Terraform plan step is successfully completed, you then can manually apply the changes after reviewing the plan. The plan is also available as an artifact so you can download it and view it locally.

![tg_3](/images/blogimages/2021-terratag-env0/tg_3.png)

### Labels on GCP
As we mentioned before, labeling your resources has a lot of technical, operations and business benefits. This blog focuses on the cost aspect.

First, let’s see that the VM we’ve created is actually tagged correctly. 

Let’s head out to the Google Cloud console. We’ll go to the Compute Engine page and, under VM, search for the VM we’ve just created. Then we’ll  go into the VM Instance details page and see that the label exists with the right value.

![tg_4](/images/blogimages/2021-terratag-env0/tg_4.png)

Now we can go to the Billing and select Reports. On the right hand side of the page, we’ll see the Filters. Under labels, we can filter the label key and the label value and get the cost of those resources.

![tg_5](/images/blogimages/2021-terratag-env0/tg_5.png)

### Summary

Tags and labels play a crucial role in managing a large scale infrastructure and offer significant benefits tilizing tools such as [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/), and [Terratag](https://www.terratag.io/) has demonstrated advantages to ease the move when using Terraform. This will also help you create a standard in your organization when it comes to tags and labels, eliminating the need for human intervention or a large-scale project to change your current Terraform code base. 

Feel free to check out the [code base](https://gitlab.com/env0/terratag-blog-post) and leave us feedback and comments.

